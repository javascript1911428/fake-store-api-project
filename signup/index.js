const signupForm = document.getElementById('signup-form');
const firstname = document.getElementById('firstname');
const lastname = document.getElementById('lastname');
const email = document.getElementById('email');
const password = document.getElementById('password');
const confirmPassword = document.getElementById("confirmPassword");
const firstnameError = document.getElementById('firstname-error');
const lastnameError = document.getElementById('lastname-error');
const emailError = document.getElementById('email-error');
const passwordError = document.getElementById('password-error');
const confirmPasswordError = document.getElementById('confirm-password-error');
const signupButton = document.getElementById("signup-button");
const homeButton = document.getElementById("home-button");


signupForm.addEventListener('submit', function (e) {
    e.preventDefault();
    clearErrors();

    if (!validateForm()) {
        return;
    } else {
        signupButton.addEventListener("click", (event) => {
            event.preventDefault();
            window.location.href = "../logiin/index.html";
        })
    }
});

function validateForm() {
    let isValid = true;
    
    if (firstname.value.trim() === '') {
        firstnameError.textContent = 'First name is required';
        isValid = false;
    } 
    
    if(!isValidName(firstname.value.trim())) {
        firstnameError.textContent = "Invalid name";
        isValid = false;
    }

    if (lastname.value.trim() === '') {
        lastnameError.textContent = 'Last name is required';
        isValid = false;
    } 

    if(!isValidName(lastname.value.trim())) {
        lastnameError.textContent = "Invalid name";
        isValid = false;
    }

    if (email.value.trim() === '') {
        emailError.textContent = 'Email is required';
        isValid = false;
    } else if (!isValidEmail(email.value.trim())) {
        emailError.textContent = 'Invalid email format';
        isValid = false;
    }

    if (password.value.trim() === '') {
        passwordError.textContent = 'Password is required';
        isValid = false;
    } 
    if (password.value.length < 6) {
        passwordError.textContent = 'Password must be at least 6 characters long';
        isValid = false;
    }
    const specialCharPattern = /[!@#$%^&*()_+{}\[\]:;<>,.?~\\\-]/;

    if(!specialCharPattern.test(password.value.trim())) {
        passwordError.textContent = "Invalid Password";
        isValid = false;
    }

    if(confirmPassword.value.trim() !== password.value.trim()) {
        confirmPasswordError.textContent = "Incorrect Password"
        isValid = false;
    }

    return isValid;
}

function isValidEmail(email) {
    // Simple email validation using a regular expression
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailPattern.test(email);
}

function isValidName(name) {
    const NamePattern = /^[A-Za-z\s]+$/;
    return NamePattern.test(name);
}


function clearErrors() {
    firstnameError.textContent = '';
    lastnameError.textContent = '';
    emailError.textContent = '';
    passwordError.textContent = '';
}

homeButton.addEventListener("click", (event) => {
    // event.preventDefault();
    window.location.href = "../index.html";
    console.log("clicked");
})

