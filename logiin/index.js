const signupForm = document.getElementById('signup-form');
const email = document.getElementById('email');
const password = document.getElementById('password');
const emailError = document.getElementById('email-error');
const passwordError = document.getElementById('password-error');
const loginButton = document.getElementById("login-button");
const homeButton = document.getElementById("home-button");


signupForm.addEventListener('submit', function (e) {
    e.preventDefault();
    clearErrors();

    if (!validateForm()) {
        return;
    } else {
        loginButton.addEventListener("click", (event) => {
            event.preventDefault();
            let messageElement = document.getElementById("message");
            messageElement.innerText = "You are login is successfull, you can now click the home button to go to home page"
            //window.location.href = "../index.html";
            console.log("everything doing well")
        })
    }
});

function validateForm() {
    let isValid = true;

    if (email.value.trim() === '') {
        emailError.textContent = 'Email is required';
        isValid = false;
    } else if (!isValidEmail(email.value.trim())) {
        emailError.textContent = 'Invalid email format';
        isValid = false;
    }

    if (password.value.trim() === '') {
        passwordError.textContent = 'Password is required';
        isValid = false;
    } 
    if (password.value.length < 6) {
        passwordError.textContent = 'Password must be at least 6 characters long';
        isValid = false;
    }
    const specialCharPattern = /[!@#$%^&*()_+{}\[\]:;<>,.?~\\\-]/;

    if(!specialCharPattern.test(password.value.trim())) {
        passwordError.textContent = "Invalid Password";
        isValid = false;
    }

    return isValid;
}

function isValidEmail(email) {
    // Simple email validation using a regular expression
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailPattern.test(email);
}

function clearErrors() {
    emailError.textContent = '';
    passwordError.textContent = '';
}

homeButton.addEventListener("click", (event) => {
    // event.preventDefault();
    window.location.href = "../index.html"
    console.log("clicked");
})