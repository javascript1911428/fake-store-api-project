let mensClothingCategoryContainer = document.getElementById("mens-clothing-container");
let jeweleryCategoryContainer = document.getElementById("jewelery-container");
let electronicsCategoryContainer = document.getElementById("electronics-container");
let womensClothingCategoryContainer = document.getElementById("womens-clothing-container");

// loader section 

window.addEventListener("load", () => {
    const loader = document.querySelector(".loader");
    console.log(loader);

    loader.classList.add("loader-hidden");

    loader.addEventListener("transitionend", () => {
        document.body.removeChild(loader);
    });
});

function createProductContainer(title, price, category, image, rating) {

    let singleProductContainer = document.createElement("div");
    singleProductContainer.classList.add("product-class");

    // creating image element conatiner
    let imageElement = document.createElement("img");
    imageElement.classList.add("image-element");
    imageElement.src = image;
    imageElement.setAttribute("title", title);

    //creating producting info container
    let productInfoContainer = document.createElement("div");
    productInfoContainer.classList.add("product-info");

    // creating price element using h3
    let priceElement = document.createElement("h3");
    priceElement.classList.add("price-element");
    priceElement.innerText = "$ " + price;

    // creating container for ratecount container 
    let rateCountConatiner = document.createElement("div");
    rateCountConatiner.classList.add("rate-count");

    // rate element
    let rateStarElement = document.createElement("div");
    rateStarElement.classList.add("rate-star");
    let rateElement = document.createElement("h3");
    rateElement.innerText = rating["rate"];

    // span element for rating
    let spanElement = document.createElement("span");
    let spanImageElement = document.createElement("img");
    spanImageElement.classList.add("span-image");
    spanImageElement.src = "https://cdn-icons-png.flaticon.com/128/9784/9784182.png";
    spanElement.append(spanImageElement);
    //spanElement.append(slashElement);
    rateElement.append(spanElement);

    rateStarElement.append(rateElement);

    // count element
    let countElement = document.createElement("h3");
    countElement.innerText = "/ " + rating["count"];

    // appending elements;

    singleProductContainer.append(imageElement);
    productInfoContainer.append(priceElement);
    rateCountConatiner.append(rateStarElement);
    rateCountConatiner.append(countElement);
    productInfoContainer.append(rateCountConatiner);
    singleProductContainer.append(productInfoContainer);

    console.log(singleProductContainer);

    return singleProductContainer;
}


fetch("https://fakestoreapi.com/products")
    .then((res) => {
        if (!res.ok) {
            throw new Error("Network response was not ok");
        }
        return res.json();
    })
    .then((json) => {
        if (json.length === 0) {
            displayNoProductsMessage();
        } else {

            for (let eachProduct of json) {
                let { id, title, price, description, category, image, rating } = eachProduct;
                let typeOfCategoryCaontainer;

                if (category === "men's clothing") {
                    typeOfCategoryCaontainer = mensClothingCategoryContainer;
                } else if (category === "jewelery") {
                    typeOfCategoryCaontainer = jeweleryCategoryContainer;
                } else if (category === "electronics") {
                    typeOfCategoryCaontainer = electronicsCategoryContainer;
                } else {
                    typeOfCategoryCaontainer = womensClothingCategoryContainer;
                }

                let productContainer = createProductContainer(title, price, category, image, rating);
                productContainer.addEventListener("click", function (event) {
                    
                    document.body.style.overflow = "hidden";

                    let productDetailsContainer = document.getElementById("product-details-container");
                    productDetailsContainer.innerHTML = "";

                    // creation of product detail 

                    let viewPageContainer = document.createElement("div");
                    viewPageContainer.style.backgroundColor = "white";
                    viewPageContainer.classList.add("product-view-container");

                    let viewImageContainer = document.createElement("img");
                    viewImageContainer.classList.add("image-element");
                    viewImageContainer.src = image;
                    viewPageContainer.append(viewImageContainer);

                    let titleElement = document.createElement("h2");
                    titleElement.innerText = title;
                    viewPageContainer.append(titleElement);

                    let viewPriceElement = document.createElement("h2");
                    viewPriceElement.innerText =  "Price: " +"$ " + price;
                    viewPageContainer.append(viewPriceElement);

                    let viewDescriptionElement = document.createElement("h4");
                    viewDescriptionElement.classList.add("view-des");
                    viewDescriptionElement.innerText = "Description: " + description;
                    viewPageContainer.append(viewDescriptionElement);

                    let viewCategoryElement = document.createElement("h2");
                    viewCategoryElement.innerText = "Category: " + category;
                    viewPageContainer.append(viewCategoryElement);

                    let viewRatingElement = document.createElement("h4");
                    viewRatingElement.innerText = "Rating: " + rating["rate"] + " /" + "Available product count: " + rating["count"];
                    viewPageContainer.append(viewRatingElement);

                    // appending the viewPageContainer to productDetailsContainer
                    productDetailsContainer.append(viewPageContainer);
                    console.log("clicked");

                    // whenver we click the certain product this will show that purticular product details
                    let modal = document.getElementById("product-modal");
                    modal.style.display = "block";

                });

                let closeButton = document.getElementsByClassName("close")[0];
                closeButton.addEventListener("click", function () {
                    let modal = document.getElementById("product-modal");
                    modal.style.display = "none";
                    document.body.style.overflow = "visible";
                });

                typeOfCategoryCaontainer.appendChild(productContainer);

            }
        }

    }).catch((error) => {
        console.log("Error:", error);
        displayErrorMessage("Failed to load products. Please try again later.");
    });

// display no products message function.

function displayNoProductsMessage() {
    let outerContainerElement = document.getElementById("outer-container");
    outerContainerElement.style.display = "none";
    let noProductsMessageContainer = document.createElement("div");
    noProductsMessageContainer.classList.add("error-message");
    noProductsMessageContainer.innerText = "NO products available";
    document.body.append(noProductsMessageContainer);
}

// dispaly error message when their is problem with api.


function displayErrorMessage(message) {
    let outerContainerElement = document.getElementById("outer-container");
    let errorMessageContainer = document.createElement("div");
    errorMessageContainer.classList.add("error-message");
    errorMessageContainer.innerText = message;
    outerContainerElement.style.display = "none";
    document.body.append(errorMessageContainer);
}

// signup jscode

let signupbtnElement = document.getElementById("signupbtn");
signupbtnElement.addEventListener("click", () => {
    window.location.href = "signup/index.html";
    console.log("clicked");
})

// login jscode

let loginbtnElement = document.getElementById("loginbtn");
loginbtnElement.addEventListener("click", () => {
    window.location.href = "logiin/index.html";
    console.log("clicked");
})